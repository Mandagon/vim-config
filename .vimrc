set number
set linebreak
set autoindent
set smarttab
set smartindent

set expandtab
set shiftwidth=4

noremap m i
noremap M I
noremap i m
noremap I M
noremap k j
noremap i k
noremap j h

noremap <C-k> <C-j>
noremap <C-i> <C-k>
noremap <C-j> <C-h>

noremap <C-K> <C-J>
noremap <C-I> <C-K>
noremap <C-J> <C-H>

noremap <C-w>k <C-w>j
noremap <C-w>i <C-w>k
noremap <C-w>j <C-w>h